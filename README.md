# Kickstart file
### IMPORTANT
This repository is a part of stack which I use to bootstrap, operate, store and version control all my work environment at various levels, such as:

1. [Operating system](https://gitlab.com/ChipotleSouthwest/kickstart)
2. [System infill (packages, parameters and so on)](https://gitlab.com/ChipotleSouthwest/ansible)
3. [Dotfiles](https://gitlab.com/ChipotleSouthwest/dotfiles)

Now this whole "project" is just the first version so any advice, ideas, suggestions or fixes are welcome. Feel free to open an issue!

This and all other repositories will be expanding as I use it.

You won't find any demonstration gifs, screenshots or videos here because I believe all of these things should be used, not watched. Also I don't think I am such good lector so I don't repeat any documentation here - you can google it by yourself.

### My  only one (for now) kickstart file
I use kickstart because I want to have repeatability, controllability and fixation at all levels of my working environment, including the OS installation phase. Of course that doesn't mean I'm utilizing every opportunity available to me. Moreover, this file does not contain some rocket science - I just wanted to have a complete stack at all levels of the my environment.

Distribution selection of the OS (as well as other options like disk partitioning) are based entirely on my personal opinion, experience and feeling - there is no guarantee that this will be relevant to you as well (as well as the contents of the rest of the repositories).

### Usage
1) first of all you should change password and username in kickstart file in `user` directive. Name is plaintext, but password should be encrypted (or you can specify it in plain by changing the parameters, but it's your responsibility). You can generate crypted password with this command:  
`mkpasswd -m yescrypt`  

2) after it  kickstart file must be exposed somewhere. Easyest way is use python. Run this in directory with kickstart file:  
`python3 -m http.server`

3) then load in network installer "Fedora Everything" (or you can use Fedora Server DVD - in this case you can omit the `url` parameter) and in grub boot menu enter the boot parameter editing section by pressing `e` on **"Install..."** line

4) add parameter below by changing IP:PORT to the address of the system where you performed step 2 (by default python expose at 8000 port) and press`Ctrl+x` - this will start the system installation  
`inst.ks=http://IP:PORT/kickstart.cfg` (you can change filename to your own)

5) installer will ask you password for LUKS encryption - enter it

6) wait until it done - system will restart automaticly

After it you can proceed to system infillation with [this](https://gitlab.com/ChipotleSouthwest/ansible) repo. These playbooks will also install my dotfiles in your system.

### Known issues
During development I tested all flow in VirtualMachines and once I had a problem with the network not working after second reboot (reboot after install, load into system and then 1 more reboot). Fixed it with `sudo nmcli networking on` . I don't know what it was.
